package shoppingCart1;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Cart implements CartDao {
	
	private List<ItemEntry> itemList;
	
	public Cart(){
		itemList = new ArrayList<>();
	}
	
    //将商品加入购物车
	public boolean add(Commodity e,int n){
		if (e == null){
			return false;
		}
		if(n>100) n=100;
		if(n<1) n=1;
		int index = findById(e.getId());
		if (index == -1){//如果不包含该商品的条目
			itemList.add(new ItemEntry(e,n));
		}
		else if(itemList.get(index).qty+n>=100) {
			itemList.get(index).setQty(100);
		}
		else{
			itemList.get(index).increase(n);
		}
		return true;
	}
	
	public boolean reduce(Integer id){//减少商品数量
		if (id==null)
			return false;
		int index = findById(id);
		if (index == -1){//未找到
			return false;
		}else{
			ItemEntry entry = itemList.get(index);
			if (entry.getQty() <= 1){//移除相关条目qty<=0，则删除条目
				itemList.remove(index);
			}else{
				entry.decrease();
			}
		}		
		return true;
	}
	
	/*输出购物车*/
	public void diplayAll(){

		int number=0;
		BigDecimal total =new BigDecimal("0");
		for (ItemEntry itemEntry : itemList) {
			System.out.println(itemEntry);
			number+=itemEntry.getQty();						
			BigDecimal a =new BigDecimal(itemEntry.getTotalPrice());
			total=total.add(a);					
		}
		System.out.printf("共%d件商品，总价：%s%n",number,total);
	}
	
	/**
	 * 通过id在购物车中查找是否已存在相关条目。
	 * 声明为private，因为只在本类中使用。
	 * @param id
	 * @return 如果包含返回相关条目所在下标，否则返回-1
	 */
	public int findById(Integer id){
		for (int i = 0; i < itemList.size(); i++) {
			if (itemList.get(i).getItem().getId().equals(id))
				return i;
		}
		return -1;
	}
	
	public List<ItemEntry> getItemList() {
		return itemList;
	}
	
	class ItemEntry{ //内部类：购物车条目类
		/**
		 * 商品
		 */
		Commodity item;
		
		/**
		 * 商品数量
		 */
		Integer qty;
		BigDecimal totalPrice;
		 
		public ItemEntry(Commodity item,int n) {
			this.item = item;
			qty = n;
		}
		
		//添加
		public void increase(int n){
			qty+=n;
		}
		
		//减少
		public void decrease(){
			qty--;
		}
		
		public Commodity getItem() {
			return item;
		}
		
		public Integer getQty() {
			return qty;
		}
		
		public void setQty(Integer qty) {
			this.qty = qty;
		}
		//计算单个条目的价格
		public String getTotalPrice() {
			DecimalFormat df = new DecimalFormat("0.00");
			BigDecimal num =new BigDecimal(qty.toString());
			BigDecimal price =new BigDecimal(item.getPrice().toString());
			this.totalPrice= num.multiply(price);
			return df.format(this.totalPrice);
		}
		
		
		@Override
		public String toString() {
			return "商品信息 [" + item + ",  数量：" + qty + ",  总价：" + getTotalPrice() + "]";
		}				
	}
	
}
