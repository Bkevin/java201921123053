package shoppingCart1;

public class Commodity {
	private Integer id;
	private String name;
	private Double price;	
		

	public Commodity(Integer id, String name, Double price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Double getPrice() {
		return price;
	}

	@Override
	public boolean equals(Object obj) {  //只比较id,id相同则说明两个商品相同
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commodity other = (Commodity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "商品编号：" + id + ",  商品名称：" + name + ",  商品单价：" + price;
	}
	
}
class Book extends Commodity{
	private String press;//出版社
	private String ISBN;//书号
	
	public Book(Integer id, String name, Double price, String publisher, String isbn) {
		super(id, name, price);
		this.press = publisher;
		this.ISBN = isbn;
	}

	public String getPublisher() {
		return press;
	}

	public String getIsbn() {
		return ISBN;
	}

	@Override
	public String toString() {
		return super.toString()+",  出版社：" + press + ",  ISBN：" + ISBN;
	}
	
}
class Computer extends Commodity{
	private String manufacturer;//生产家
	private String specs;//规格
		
	public Computer(Integer id, String name, Double price, String manufacturer, String specs) {
		super(id, name, price);
		this.manufacturer = manufacturer;
		this.specs = specs;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getSpecs() {
		return specs;
	}

	@Override
	public String toString() {
		return super.toString()+",  制造商：" + manufacturer + ",  规格：" + specs;
	}
	
}