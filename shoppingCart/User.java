package shoppingCart1;

public class User {
	static Cart cart = new Cart();
	
	/*添加商品*/
	public  static void addCommodity(Commodity x,int n) {		
		if(!cart.add(x,n))
			System.out.println("添加失败！");
	}
	
	/*减少商品数量*/
	public static void reduceCommodity(Integer id) {
		if(cart.reduce(id))
			System.out.println("该商品数量已减一");
		else
			System.out.println("减少失败！购物车中不存在编号为"+id+"的商品！");
	}
	
	/*删除商品条目*/
	public static void delItem(String... id) {
		for(String e :id) {
			int index = cart.findById(Integer.parseInt(e));
			if(index==-1) {
				System.out.println("购物车中不存编号为"+e+"的商品条目！");
			}else {
				cart.getItemList().remove(index);
				System.out.println("删除成功！");
			}
		}
		
	}
	
	/*清空购物车*/
	public static void clearCart() {
		cart.getItemList().clear();
		System.out.println("购物车已清空！");
	}
	
	/*设置某商品数量*/
	public static void setNum(Integer id,int n) {
		int index = cart.findById(id);
		if(index==-1) {
			System.out.println("购物车中不存在编号为"+id+"的商品！");
			return;
		}
		if(n<1) {
			System.out.println("输入的数量小于1，自动删除该商品条目");
			n=0;
		}else if(n>100) {
			System.out.println("输入的数量超过100！自动将数量置为100");
			n=100;
		}
		if(n <= 0) {
			User.delItem(id.toString());
		}else {
			cart.getItemList().get(index).setQty(n);
		}
		System.out.println("修改成功！");
	}
	/*输出购物车*/
	public static void displayAll() {
		if(cart.getItemList().size()==0) {
			System.out.println("购物车为空！");
		}else {
			System.out.println("当前购物车中有以下商品：");
			cart.diplayAll();
		}
	}
	
}
