package shoppingCart1;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		ArrayList<Commodity> shelves = new ArrayList<>();
		Menu.shelfInitial(shelves);

		String choice;
		Scanner sc = new Scanner(System.in);

		int itemId;
		int itemNum;
		String input;
		String[] inputDatas;

		while (true) {
			Menu.menu();
			choice = sc.nextLine().split("\\s+")[0];
			switch (choice) {
			case "1":
				Menu.show();
				break;
			case "2":
				System.out.println("请输入商品编号和数量(商品数量上限为100，只读取前两个整数,小数部分自动舍弃)：");
				inputDatas = sc.nextLine().split("\\s+");
				while (!inputDatas[0].equals("end")) {
					if (inputDatas.length < 1 || inputDatas[0].equals("")) {
						System.out.println("不合法的输入！！！请重新输入");
						break;
					}
					itemId = (int) Math.floor(Double.valueOf(inputDatas[0]));
					if (itemId > shelves.size()) {
						System.out.println("不存在编号为" + itemId + "的商品！请重新输入！");
						break;
					}
					itemNum = (int) Math.floor(Double.valueOf(inputDatas[1]));
					User.addCommodity(shelves.get(itemId - 1), itemNum);
					inputDatas = sc.nextLine().split("\\s+");
				}
				User.displayAll();
				break;
			case "3":
				System.out.print("请输入商品编号(只取第一个整数，小数部分自动舍弃)：");
				inputDatas = sc.nextLine().split("\\s+");
				if (inputDatas.length < 1 || inputDatas[0].equals("")) {
					System.out.println("不合法的输入！！！请重新输入");
					break;
				}
				itemId = (int) Math.floor(Double.valueOf(inputDatas[0]));

				User.reduceCommodity(itemId);
				User.displayAll();
				break;
			case "4":
				System.out.print("请输入商品编号(可输入多个整数同时删除多个商品条目，小数部分自动舍弃)：");
				inputDatas = sc.nextLine().split("\\s+");
				if (inputDatas.length < 1 || inputDatas[0].equals("")) {
					System.out.println("不合法的输入！！！请重新输入");
					break;
				}
				User.delItem(inputDatas);
				User.displayAll();
				break;
			case "5":
				System.out.print("请输入商品编号和目标数量(只取前两个整数，小数部分自动舍弃)：");
				inputDatas = sc.nextLine().split("\\s+");
				if (inputDatas.length < 1 || inputDatas[0].equals("")) {
					System.out.println("不合法的输入！！！请重新输入");
					break;
				}
				itemId = (int) Math.floor(Double.valueOf(inputDatas[0]));
				itemNum = (int) Math.floor(Double.valueOf(inputDatas[1]));
				User.setNum(itemId, itemNum);
				User.displayAll();
				break;
			case "6":
				User.clearCart();
				break;
			case "7":
				User.displayAll();
				break;
			case "8":
				System.out.println("谢谢惠顾，欢迎下次光临！");
				sc.close();
				System.exit(0);
			default:
				System.out.println("不合法的输入！！！请重新输入");
				break;
			}
			System.out.printf("\n\n\n");

		}

	}

}
