package shoppingCart1;

public interface CartDao {
	public boolean add(Commodity e,int n);
	public boolean reduce(Integer id);
	public void diplayAll();
	public int findById(Integer id);
}
