package shoppingCart1;

import java.util.ArrayList;

public class Menu {
	static Commodity x1,x2,x3,x4;
	
	static {
		x1 = new Book(1, "JAVA学习笔记",49.50,"清华大学出版社","9787302501183");
		x2 = new Book(2, "通信原理",23.85,"国防工业出版社","9787302501184");
		x3 = new Computer(3, "Macbook",18425.20,"Apple","i7 8750");
		x4 = new Computer(4, "Macbook pro",22482.42,"Apple","i9 8750");
	}
	
	public static void shelfInitial(ArrayList<Commodity> shelves) {
		shelves.add(x1);
		shelves.add(x2);
		shelves.add(x3);
		shelves.add(x4);
	}
	
	public static void menu() {
		System.out.println("*************功能选择**************");
		System.out.println("*   <1>展示所有商品               *");
		System.out.println("*   <2>添加商品                   *");
		System.out.println("*   <3>商品数量减一               *");
		System.out.println("*   <4>删除商品条目               *");
		System.out.println("*   <5>修改购物车中指定商品的数量 *");
		System.out.println("*   <6>清空购物车                 *");
		System.out.println("*   <7>输出购物车                 *");
		System.out.println("*   <8>退出程序                   *");
		System.out.println("***********************************");
		System.out.print("请输入你的选择:");
	}
	
	public static void show() {
		System.out.println("商城中有以下商品：");
		System.out.println(x1);
		System.out.println(x2);
		System.out.println(x3);
		System.out.println(x4);
	}
	
}
